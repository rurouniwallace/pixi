﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Input.Commands
{
    /// <summary>
    /// A basic input command
    /// </summary>
    /// <param name="time">game timing state</param>
    public delegate void Command(GameTime time);
}
