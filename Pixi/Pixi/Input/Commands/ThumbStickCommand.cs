﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Input.Commands
{
    /// <summary>
    /// Command triggered by pressing thumb sticks
    /// </summary>
    /// <param name="previousPosition">Position of the thumb stick on the previous frame. Always null on the first frame/param>
    /// <param name="position">position of the thumb stick, floating-point value from -1.0 to 1.0</param>
    /// <param name="time">game timing state</param>
    public delegate void ThumbStickCommand(Vector2? previousPosition, Vector2 position, GameTime time);
}
