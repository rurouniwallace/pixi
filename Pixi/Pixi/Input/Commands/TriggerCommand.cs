﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Input.Commands
{
    /// <summary>
    /// Command triggered by pressing trigger buttons
    /// </summary>
    /// <param name="position">trigger position</param>
    /// <param name="time">game timing state</param>
    public delegate void TriggerCommand(float position, GameTime time);
}
