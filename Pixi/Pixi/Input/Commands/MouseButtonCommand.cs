﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Input.Commands
{
    /// <summary>
    /// Command trigger by a mouse click
    /// </summary>
    /// <param name="x">x-axis coordinate of mouse cursor at the instant of the click</param>
    /// <param name="y">y-axis coordinate of mouse cursor at the instant of the click</param>
    /// <param name="time">game timing state</param>
    public delegate void MouseButtonCommand(int x, int y, GameTime time);
}
