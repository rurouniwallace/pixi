﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Graphics
{
    /// <summary>
    /// A registry for completable graphical elements. This dispatcher keeps track of all elements that
    /// are designed to be displayed for a fixed amount of time, and removes them once complete
    /// </summary>
    public class CompletableDispatcher : ICompletableDispatcher
    {
        /// <summary>
        /// A set of completables graphical elements to execute. This must be a set rather than a list.
        /// That way if a completable is re-registered, it won't get updated and drawn twice
        /// </summary>
        private ISet<ICompletable> _completables;

        /// <summary>
        /// Construct a new instance
        /// </summary>
        public CompletableDispatcher()
        {
            _completables = new HashSet<ICompletable>();
        }

        /// <summary>
        /// Register an additional completable, adding it to the queue
        /// </summary>
        /// <param name="completable">Completable to add</param>
        public void registerCompletable(ICompletable completable)
        {
            _completables.Add(completable);
        }

        /// <summary>
        /// Update all in-progress completables
        /// </summary>
        /// <param name="time">Game timing state</param>
        public void OnUpdate(GameTime time)
        {
            foreach (ICompletable completable in _completables)
            {
                if (completable.Status == CompletionStatus.IN_PROGRESS)
                {
                    completable.OnUpdate(time);
                }


            }
        }

        /// <summary>
        /// Draw all in-progress completables
        /// </summary>
        /// <param name="time">Game timing state</param>
        public void OnDraw(GameTime time)
        {
            foreach (ICompletable completable in _completables)
            {
                if (completable.Status == CompletionStatus.IN_PROGRESS)
                {
                    completable.OnDraw(time);
                }
            }
        }
    }
}
