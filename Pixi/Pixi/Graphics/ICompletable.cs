﻿using Microsoft.Xna.Framework;

namespace Pixi.Graphics
{
    /// <summary>
    /// Abstraction for a completable graphical element (e.g. an animation that runs for a fixed amount of time)
    /// </summary>
    public interface ICompletable
    {
        /// <summary>
        /// The status of the completable
        /// </summary>
        public CompletionStatus Status { get; }

        /// <summary>
        /// Actions to take on frame updates
        /// </summary>
        /// <param name="time">Game timing state</param>
        public void OnUpdate(GameTime time);

        /// <summary>
        /// Draw the completable element
        /// </summary>
        /// <param name="time">Game timing state</param>
        public void OnDraw(GameTime time);
        
        /// <summary>
        /// Start the completable
        /// </summary>
        public void Start();

        /// <summary>
        /// Terminate the completable
        /// </summary>
        public void Terminate();
    }
}