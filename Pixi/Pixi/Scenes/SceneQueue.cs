﻿using Microsoft.Xna.Framework;
using Pixi.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Scenes
{
    /// <summary>
    /// Queues up scenes and handles starting and stopping them
    /// </summary>
    public class SceneQueue : ISceneQueue
    {
        /// <summary>
        /// Queued up scenes
        /// </summary>
        private Queue<ICompletable> _queue;

        /// <summary>
        /// Currently running scene, if any
        /// </summary>
        private ICompletable _currentScene;

        /// <summary>
        /// Construct a new instance
        /// </summary>
        public SceneQueue()
        {
            _queue = new Queue<ICompletable>();
        }

        /// <summary>
        /// Update the current scene. If the current scene is complete, dequeue the next scene
        /// and make it the current
        /// </summary>
        /// <param name="time">Game timing state</param>
        public void UpdateCurrentScene(GameTime time)
        {
            if (_currentScene == null || _currentScene.Status == CompletionStatus.COMPLETE)
            {
                if (_currentScene != null)
                {
                    _currentScene.Terminate();
                }
                if (_queue.Count > 0)
                {
                    _currentScene = _queue.Dequeue();
                    _currentScene.Start();
                }
                else
                {
                    _currentScene = null;
                }
            }

            if (_currentScene != null)
            {
                _currentScene.OnUpdate(time);
            }
        }

        /// <summary>
        /// Draw the current scene
        /// </summary>
        /// <param name="time">Game timing state</param>
        public void DrawCurrentScene(GameTime time)
        {
            if (_currentScene != null)
            {
                _currentScene.OnDraw(time);
            }
        }

        /// <summary>
        /// Add a new scene to the queue
        /// </summary>
        /// <param name="newScene">new scene to add to the queue</param>
        public void AddSceneToQueue(Scene newScene)
        {
            _queue.Enqueue(newScene);
        }
    }
}
