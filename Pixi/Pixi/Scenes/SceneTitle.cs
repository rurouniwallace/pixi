﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Pixi.Graphics;
using Pixi.Input;
using Pixi.Input.Commands;
using Pixi.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Scenes
{
    class SceneTitle : Scene
    {
        /// <summary>
        /// Title screen menu options
        /// </summary>
        private IList<String> _titleScreenSelections;

        /// <summary>
        /// Currently selected menu option
        /// </summary>
        private int _currentSelection;

        /// <summary>
        /// Scene dispatcher. Used to switch to a new scene when necessary
        /// </summary>
        private ISceneQueue _sceneQueue;

        /// <summary>
        /// Input listener
        /// </summary>
        private IListensForInput _inputListener;

        /// <summary>
        /// Game content manager
        /// </summary>
        private ContentManager _contentManager;

        /// <summary>
        /// Handles the dispatching of completable elements
        /// </summary>
        private ICompletableDispatcher _completableDispatcher;

        /// <summary>
        /// Assists in drawing 2D bitmaps with a given set of settings
        /// </summary>
        private SpriteBatch _spriteBatch;

        /// <summary>
        /// Utility to exit the game
        /// </summary>
        private IExitsGame _gameExiter;

        /// <summary>
        /// Number of seconds to wait before scrolling to the next menu option while one of the scroll buttons is held
        /// </summary>
        private const double _scrollDelaySeconds = 0.3;

        /// <summary>
        /// Number of pixels to render between each menu selection
        /// </summary>
        private const int _selectionsMargin = 10;

        /// <summary>
        /// Starting vertical position to render menu options
        /// </summary>
        private const int menuYPosition = 365;

        /// <summary>
        /// Current number of seconds since last scroll
        /// </summary>
        private double _secondsSinceLastScroll;
        
        /// <summary>
        /// Background image
        /// </summary>
        private Texture2D _background;

        /// <summary>
        /// Font to use to render menu items
        /// </summary>
        private SpriteFont _menuFont;

        /// <summary>
        /// Flashing indicator to show the user that a game pad was connected
        /// </summary>
        private Flasher _gamePadConnectedIndicator;

        /// <summary>
        /// Flashing indicator to show the user that a game pad was disconnected
        /// </summary>
        private Flasher _gamePadDisconnectedIndicator;

        /// <summary>
        /// Construct a new instance
        /// </summary>
        /// <param name="sceneQueue">Scene dispatcher, used to switch to another scene</param>
        /// <param name="inputListener">Input listener</param>
        /// <param name="contentManager">Game content manager</param>
        /// <param name="spriteBatch">Sprite renderer</param>
        public SceneTitle(ISceneQueue sceneQueue, IListensForInput inputListener, ContentManager contentManager, SpriteBatch spriteBatch, IExitsGame gameExiter, ICompletableDispatcher completableDispatcher)
        {
            _sceneQueue = sceneQueue;
            _inputListener = inputListener;
            _contentManager = contentManager;
            _spriteBatch = spriteBatch;
            _gameExiter = gameExiter;
            _completableDispatcher = completableDispatcher;

            _titleScreenSelections = new List<String>();
            _titleScreenSelections.Add("New Game");
            _titleScreenSelections.Add("Load Game");
            _titleScreenSelections.Add("Exit Game");

            _background = _contentManager.Load<Texture2D>("Scenes/Title/Graphics/background");

            _menuFont = _contentManager.Load<SpriteFont>("Fonts/TastefulSandwich");
        }

        /// <summary>
        /// Actions to take upon starting the scene
        /// </summary>
        override
        public void Start()
        {
            _currentSelection = 0;

            this._resetScrollDelay();

            Command scrollUpCommand = delegate (GameTime time) {
                this._scrollUp(time);
            };

            Command scrollDownCommand = delegate (GameTime time)
            {
                this._scrollDown(time);
            };

            Command stopScrollingCommand = delegate (GameTime time)
            {
                this._resetScrollDelay();
            };

            Command chooseCurrentSelectionCommand = delegate (GameTime time)
            {
                this._chooseCurrentSelection();
            };

            Dictionary<Keys, Command> keyboardPressBindings = new Dictionary<Keys, Command>();
            keyboardPressBindings.Add(Keys.Up, scrollUpCommand);
            keyboardPressBindings.Add(Keys.Down, scrollDownCommand);
            keyboardPressBindings.Add(Keys.Enter, chooseCurrentSelectionCommand);

            Texture2D gamePadIcon = _contentManager.Load<Texture2D>("Icons/gamepad_white");
            Texture2D gamePadDisconnectedIcon = _contentManager.Load<Texture2D>("Icons/gamepad_white_disconnected");

            _gamePadConnectedIndicator = new Flasher(0.5, 4, _spriteBatch, gamePadIcon);
            float gamePadConnectedIndicatorPositionX = _spriteBatch.GraphicsDevice.Viewport.Width - _gamePadConnectedIndicator.Width;
            float gamePadConnectedIndicatorPositionY = _spriteBatch.GraphicsDevice.Viewport.Height - _gamePadConnectedIndicator.Height;

            _gamePadConnectedIndicator.ScreenPosition = new Vector2(gamePadConnectedIndicatorPositionX, gamePadConnectedIndicatorPositionY);

            _gamePadDisconnectedIndicator = new Flasher(0.5, 4, _spriteBatch, gamePadDisconnectedIcon);
            float gamePadDisconnectedIndicatorPositionX = _spriteBatch.GraphicsDevice.Viewport.Width - _gamePadDisconnectedIndicator.Width;
            float gamePadDisconnectedIndicatorPositionY = _spriteBatch.GraphicsDevice.Viewport.Height - _gamePadDisconnectedIndicator.Height;
            _gamePadDisconnectedIndicator.ScreenPosition = new Vector2(gamePadDisconnectedIndicatorPositionX, gamePadDisconnectedIndicatorPositionY);

            _completableDispatcher.registerCompletable(_gamePadConnectedIndicator);
            _completableDispatcher.registerCompletable(_gamePadDisconnectedIndicator);

            Command flashGamePadConnectedIndicator = delegate (GameTime time)
            {
                if (_gamePadDisconnectedIndicator.Status == CompletionStatus.IN_PROGRESS)
                {
                    _gamePadDisconnectedIndicator.Terminate();
                }
                _gamePadConnectedIndicator.Start();
            };

            Command flashGamePadDisconnectedIndicator = delegate (GameTime time)
            {
                if (_gamePadConnectedIndicator.Status == CompletionStatus.IN_PROGRESS)
                {
                    _gamePadConnectedIndicator.Terminate();
                }
                _gamePadDisconnectedIndicator.Start();
            };

            _inputListener.KeyPressBindings = keyboardPressBindings;

            Dictionary<Keys, Command> keyboardReleaseBindings = new Dictionary<Keys, Command>();
            keyboardReleaseBindings.Add(Keys.Up, stopScrollingCommand);
            keyboardReleaseBindings.Add(Keys.Down, stopScrollingCommand);

            _inputListener.KeyReleaseBindings = keyboardReleaseBindings;

            _completableDispatcher.registerCompletable(_gamePadConnectedIndicator);

            Dictionary<PlayerIndex, Command> gamePadConnectedBindings = new Dictionary<PlayerIndex, Command>();
            gamePadConnectedBindings[PlayerIndex.One] = flashGamePadConnectedIndicator;

            Dictionary<PlayerIndex, Command> gamePadDisconnectedBindings = new Dictionary<PlayerIndex, Command>();
            gamePadDisconnectedBindings[PlayerIndex.One] = flashGamePadDisconnectedIndicator;

            _inputListener.GamePadConnectedBinding = gamePadConnectedBindings;
            _inputListener.GamePadDisconnectedBinding = gamePadDisconnectedBindings;


            _inputListener.GamePadUpButtonPressBinding = BuildGamePadBinding<Command>(scrollUpCommand);
            _inputListener.GamePadDownButtonPressBinding = BuildGamePadBinding<Command>(scrollDownCommand);
            _inputListener.GamePadAButtonPressBinding = BuildGamePadBinding<Command>(chooseCurrentSelectionCommand);

            _inputListener.GamePadUpButtonReleaseBinding = BuildGamePadBinding<Command>(stopScrollingCommand);
            _inputListener.GamePadDownButtonReleaseBinding = BuildGamePadBinding<Command>(stopScrollingCommand);

            ThumbStickCommand scrollWithThumbstickCommand = delegate (Vector2? previousThumbstickPosition, Vector2 thumbstickPosition, GameTime time)
            {
                if (thumbstickPosition.Y > 0.0f)
                {
                    _scrollUp(time);
                }
                else if (thumbstickPosition.Y < 0.0f)
                {
                    _scrollDown(time);
                } else if (previousThumbstickPosition != null && previousThumbstickPosition.Value.Y != 0.0f && thumbstickPosition.Y == 0.0f)
                {
                    _resetScrollDelay();
                }
            };

            _inputListener.GamePadLeftThumbStickBinding = BuildGamePadBinding<ThumbStickCommand>(scrollWithThumbstickCommand);
        }

        /// <summary>
        /// Scroll up in menu
        /// </summary>
        /// <param name="time">game timing state</param>
        private void _scrollUp(GameTime time)
        {

            _secondsSinceLastScroll += time.ElapsedGameTime.TotalSeconds;

            // set a delay to make sure we're not scrolling too quickly (i.e. every screen refresh)
            if (_secondsSinceLastScroll >= _scrollDelaySeconds)
            {
                if (_currentSelection == 0)
                {
                    _currentSelection = _titleScreenSelections.Count - 1;
                }
                else
                {
                    _currentSelection -= 1;
                }
                _secondsSinceLastScroll = 0.0;
            }
        }

        /// <summary>
        /// Scroll down in menu
        /// </summary>
        /// <param name="time">game timing state</param>
        private void _scrollDown(GameTime time)
        {
            _secondsSinceLastScroll += time.ElapsedGameTime.TotalSeconds;

            // set a delay to make sure we're not scrolling too quickly (i.e. every screen refresh)
            if (_secondsSinceLastScroll >= _scrollDelaySeconds)
            {
                if (_currentSelection == _titleScreenSelections.Count - 1)
                {
                    _currentSelection = 0;
                }
                else
                {
                    _currentSelection += 1;
                }
                _secondsSinceLastScroll = 0.0;
            }
        }

        /// <summary>
        /// Reset current scroll delay
        /// </summary>
        private void _resetScrollDelay()
        {
            _secondsSinceLastScroll = Double.PositiveInfinity;
        }

        /// <summary>
        /// Select the current menu selection
        /// </summary>
        private void _chooseCurrentSelection()
        {
            switch (_currentSelection)
            {
                case 2:
                    _gameExiter.ExitGame();
                    break;
            }
        }

        /// <summary>
        /// Actions to take on each frame update
        /// </summary>
        /// <param name="time">Game timing state</param>
        override
        public void OnUpdate(GameTime time)
        {
            _completableDispatcher.OnUpdate(time);

           _inputListener.ExecuteCommands(time);
        }

        /// <summary>
        /// Draw the title screen
        /// </summary>
        /// <param name="time">Game timing state</param>
        override
        public void OnDraw(GameTime time)
        {
            _spriteBatch.Begin();

            int viewportWidth = _spriteBatch.GraphicsDevice.Viewport.Width;
            int viewportHeight = _spriteBatch.GraphicsDevice.Viewport.Height;

            int backgroundWidth = _background.Width;
            int backgroundHeight = _background.Height;

            _spriteBatch.Draw(_background, new Vector2((viewportWidth / 2) - (backgroundWidth / 2), (viewportHeight / 2) - (backgroundHeight / 2)), Color.White);

            int currentYPosition = menuYPosition;
            int selectionIndex = 0;
            foreach (String menuOption in _titleScreenSelections)
            {
                Vector2 textSize = _menuFont.MeasureString(menuOption);

                Color fontColor = Color.White;
                if (selectionIndex == _currentSelection)
                {
                    fontColor = Color.Yellow;
                }

                _spriteBatch.DrawString(_menuFont, menuOption, new Vector2((viewportWidth / 2) - (textSize.X / 2), currentYPosition), fontColor);

                currentYPosition += (int)textSize.Y + _selectionsMargin;

                selectionIndex++;
            }

            _completableDispatcher.OnDraw(time);

            _spriteBatch.End();
        }

        /// <summary>
        /// Actions to take upon stopping the scene
        /// </summary>
        override
        public void Terminate()
        {
            base.Terminate();
            _inputListener.ClearAllBindings();
        }

        override
        protected void Reset()
        {

        }
    }
}
