﻿namespace Pixi.Util
{
    /// <summary>
    /// Abstraction for utility that exits the game
    /// </summary>
    public interface IExitsGame
    {
        /// <summary>
        /// Exit the game
        /// </summary>
        public void ExitGame();
    }
}